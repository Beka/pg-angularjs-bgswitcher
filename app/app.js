angular.module('MyApp', []);

angular.module('MyApp').controller('MyController', function() {
   var mc = this;
});

angular.module('MyApp').directive('backgroundSlider', ['$interval', function($interval) {
    return {
        restrict: 'E',
        link: function(scope, element, attrs) {
            var images = $(element[0]).find('img'),
                currentImage = 0;
            
            // init first image
            images[currentImage].classList.add('active');
            
            // function to switch the image
            function switchImage() {
                if (images[currentImage].classList.contains('active')) {
                    images[currentImage].classList.remove('active');
                    currentImage++;
                    
                    if (currentImage < images.length) {
                        images[currentImage].classList.add('active');
                    }
                    else {
                        currentImage = 0;
                        images[currentImage].classList.add('active');
                    }
                }
            }
            
            // Start the magic here
            $interval(function() {
                switchImage();
            }, 5000);
        }
    };
}]);